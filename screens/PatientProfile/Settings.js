import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native';
import { LinearGradient } from 'expo'
import { Actions } from 'react-native-router-flux';
import moment from 'moment'
import { colors, fontSizes, gradients } from '../../constants/styles'

import Switch from '../../components/Switch'
import Checkbox from '../../components/Checkbox'
import TextField from '../../components/TextField'
import BottomNavigation from '../../components/BottomNavigation'
import Button from '../../components/Button'
import CreditCard from '../../components/CreditCard'

import Arrow from '../../assets/images/right_arrow.png'

export default class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      cardVisible: false
    }
  }

  _onSwitchOnDemandConsultation = (state) => {
    //true / false
  }

  _onToggleAudioCall = (state) => {

  }

  _onToggleVideoCall = (state) => {

  }

  _onToggleChat = (state) => {

  }

  _onConfig = ()  => {

  }

  saveCardDetails(){
    this.setState({cardVisible: true});
  }

  removeCardDetails(){
    Actions.remove_card();
  }

  renderEnterCreditCardDetails(){
    return(
      <View>
        <View style={styles['form']}>
          
          <View style={styles['formItem']}>
            <TextField placeholder='Name on your card'></TextField>
          </View>
          <View style={styles['formItem']}>
            <TextField placeholder='Enter the credit card number' ></TextField>
          </View>
          <View style={styles['formItemTextRow']}>
            <TextField
              
              style={{flex: 1, marginRight: 8}}
              inputStyle={{}}
              placeholder="Expiry Date"/>
            <TextField
              
              style={{flex: 1, marginLeft: 8}}
              inputStyle={{}}
              placeholder="CVV"/>
          </View>
        </View>
        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this.saveCardDetails.bind(this)} >Save card details</Button>
        </LinearGradient>
      </View>
      )
  }

  renderCurrentBalance(){
    const card = {
      balance: "$954.55",
      number: "0068",
      expiry: "09/17"
    }
    return(
      <View>
        <CreditCard style={{marginTop: 24}} card={card}/>
        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this.removeCardDetails.bind(this)} background="transparent">Remove card details from account</Button>
        </LinearGradient>
      </View>
      )
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{marginBottom: 72}} contentInset={{top: 0, bottom: 64}}>
          {this.state.cardVisible || <View>
            {this.renderEnterCreditCardDetails()}
          </View>
        }
        {this.state.cardVisible && <View>
            {this.renderCurrentBalance()}
          </View>
        }
        
        </ScrollView>
        
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  title: {
    fontSize: 20,
    color: colors.dark.text,
    marginVertical: 4
  },
  subtitle: {
    color: colors.text,
    marginVertical: 4,
    width: 200,
  },
  headerCard: {
    paddingHorizontal: 24,
    paddingVertical: 4
  },
  back: {
    flexDirection: 'row',
    marginBottom: 8
  },
  form: {
    paddingTop: 16,
    paddingHorizontal: 24
  },
  bottomCard: {
    padding: 24,
    justifyContent: 'center'
  },
  formItem: {
    marginVertical: 8
  },
  formItemTextRow:{
    marginVertical: 8,
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    flex: 1
  },
  label: {
    paddingVertical: 4,
    color: colors.text,
    fontSize: 13
  }
})
