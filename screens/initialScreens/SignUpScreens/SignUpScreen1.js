import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';

import { LinearGradient } from 'expo'
import { colors, fontSizes, gradients } from '../../../constants/styles'
import Dropdown from '../../../components/Dropdown'
import TextField from '../../../components/TextField'
import Button from '../../../components/Button'

import RangeSlider from '../../../components/RangeSlider'
import ProgressWizard from '../../../components/ProgressWizard'

import { Actions } from 'react-native-router-flux';
import * as actions from '../../../actions';
import { connect } from 'react-redux';

class SignUpScreen1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      gender: ''
    }
    this.handleChangeValue = this.handleChangeValue.bind(this);
    this._submitNext = this._submitNext.bind(this);
  }

  static navigationOptions = {
    //TODO
  };

  _submitNext(){
    console.log("SignUpScreen1");
    this.setState({first_name: first_name, last_name: last_name, email: email, gender: gender});
    let first_name, last_name, email, gender;

    let signupData = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      gender: this.state.gender
    }
    this.props.store_input2(signupData);
    Actions.signup2();
  }

  handleChangeValue(field_name, textInput){
   // console.log(field_name, textInput);
    let textValue = textInput;
    let field_value = field_name;
    if( field_value == "first_name"){
      this.setState({first_name: textValue});
    }
    else if( field_value == "last_name"){
      this.setState({last_name: textValue});
    }
    else if( field_value == "email"){
      this.setState({email: textValue});
    }     
  }

  render() {
    var gender = [
      {title: 'Male'},
      {title: 'Female'}
    ]
    
    const overlay = this.state.submitted ? (
      <View style={styles.overlay}></View>
    ) : null

    return (
      <View style={styles['container']}>
        
        <LinearGradient
          style={styles['headerCard']}
          colors={gradients.grayWhite}>
          <Text style={styles['title']}>Create a new account</Text>
          <Text style={styles['subtitle']}>Create an account with new phone number</Text>
          <View style={{marginTop: 32, flexDirection: 'row', justifyContent: 'center'}}>
            <ProgressWizard step={1}/>
          </View>
        </LinearGradient>
        
        <View style={styles['form']}>
          <View style={styles['formItemTextRow']}>
            <TextField
              label="FIRST NAME"
              style={{flex: 1, marginRight: 8}}
              inputStyle={{}}
              value={this.state.first_name}
              onChange={this.handleChangeValue}
              prop_name="first_name"
              placeholder="First name"/>
            <TextField
              label="LAST NAME"
              style={{flex: 1, marginLeft: 8}}
              inputStyle={{}}
              prop_name="last_name"
              value={this.state.last_name}
              onChange={this.handleChangeValue}
              placeholder="Last name"/>
          </View>
          <View style={styles['formItem']}>
            <TextField 
              placeholder='Enter your Email' 
              label="EMAIL"
              prop_name="email"
              value={this.state.email}
              onChange={this.handleChangeValue}
              />
          </View>
          <View style={styles['formItem']}>
            <Text style={styles['label']}>GENDER</Text>
            <Dropdown title={'Gender'} list={gender} onSelect={(obj,i) => this.setState({gender: obj.title})}></Dropdown>
          </View>
        </View>

        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this._submitNext} >Continue</Button>
        </LinearGradient>
        {overlay}
      </View>
    )
  }
}

export default connect(null, actions)(SignUpScreen1);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: colors.white
  },
  title: {
    fontSize: 20,
    color: colors.dark.text,
    marginVertical: 4
  },
  subtitle: {
    color: colors.text,
    marginVertical: 4,
    width: 200,
  },
  headerCard: {
    paddingHorizontal: 24,
    paddingVertical: 4
  },
  back: {
    flexDirection: 'row',
    marginBottom: 8
  },
  form: {
    paddingHorizontal: 24
  },
  bottomCard: {
    padding: 24,
    justifyContent: 'center'
  },
  formItem: {
    marginVertical: 8
  },
  formItemTextRow:{
    marginVertical: 80,
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    flex: 1
  },
  label: {
    paddingVertical: 4,
    color: colors.text,
    fontSize: 13
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 10,
    backgroundColor: colors.fadedWhiteText
  }
})