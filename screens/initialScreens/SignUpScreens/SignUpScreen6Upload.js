import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native';

import { LinearGradient } from 'expo'
import { Actions } from 'react-native-router-flux';

import { DocumentPicker, ImagePicker } from 'expo';
import { colors, fontSizes, gradients } from '../../../constants/styles'
import LeftArrow from '../../../assets/images/left_arrow.png'
import Button from '../../../components/Button'
import ProgressWizard from '../../../components/ProgressWizard'
import ProgressCircle from '../../../components/ProgressCircle'

import * as actions from '../../../actions';
import { connect } from 'react-redux';

class SignUpScreen6Upload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }

    this._onContinue = this._onContinue.bind(this);
  //  this._onCancelUpload = this._onCancelUpload.bind(this);
  }

  static navigationOptions = {
    //TODO
  };

  _onContinue() {
    //store in firebase;
    this.props.store_signup();
    Actions.doctor_specialities();
  }

  _UploadFile = async () => {
    let result = await DocumentPicker.getDocumentAsync({});
    console.log(result, result.uri);
    if(result.type == "success"){
      this.props.uploadAsFile(result.uri);
    }
  }

  _prevScreen(){
    // Actions.signup4();
  }

  render() {
    return (
      <View style={styles['container']}>
        <LinearGradient colors={gradients['grayWhite']} style={styles['topCard']}>
          <Text style={styles['title']}>Create a new account</Text>
          <Text style={{color: colors.text, width: 200}}>Create an account with new phone number </Text>
          <View style={{marginTop: 32, flexDirection: 'row', justifyContent: 'center'}}>
            <ProgressWizard step={4}/>
          </View>
        </LinearGradient>
        <ScrollView style={styles['form']}>
         <TouchableOpacity style={styles.back} onPress={this._prevScreen}>
          <Image source={LeftArrow} style={{marginRight: 8}}/>
          <Text style={{color: colors.blue, fontSize: fontSizes['sm']}}>BACK</Text>
         </TouchableOpacity>

         <View style={styles['body']}>
          <Text style={{color: colors.dark.text, marginVertical: 10}}>Upload your certificates</Text>
          <Text style={{color: colors.text}}>
            Your certificates will not be shared with anyone. This is purely for our internal use to verify your identity.
          </Text>
          <Button style={{marginVertical: 20}} onPress={this._UploadFile} size='lg'>Upload more files from phone</Button>
          <View style={styles['uploads']}>

            <View style={styles['upload']}>
              <Image style={styles['upload_icon']} source={require('../../../assets/images/SquareIconPDF.png')} />
              <Text style={styles['upload_title']}>Master degree certificate.pdf</Text>
            </View>

            <View style={styles['upload']}>
              <Image style={styles['upload_icon']} source={require('../../../assets/images/SquareIconPDF.png')} />
              <Text style={styles['upload_title']}>Address proof.pdf</Text>
            </View>

          </View>
          </View>
        </ScrollView>
        
        <LinearGradient colors={gradients['whiteGray']} style={styles['bottomCard']}>
          <Button size='lg' onPress={this._onContinue}>Continue</Button>
        </LinearGradient>
      </View>
    )
  }
}

function mapStateToProps({fetchInput}){
  if(fetchInput != null){
    console.log("fetchInput.....", fetchInput.signup_data);
     return fetchInput
  }
  return {test: "test"}
}
export default connect(mapStateToProps, actions)(SignUpScreen6Upload);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: colors.white,
    paddingBottom: 10
  },
  topCard: {
    paddingHorizontal: 24,
    paddingTop: 16,
    flex: 1.5
  },
  body:{
    paddingTop: 40, //TODO
    flex: 4,
    paddingHorizontal: 8,
    backgroundColor: colors.white
  },
  form:{
    paddingHorizontal: 24
  },
  bottomCard: {
    flex: 0.8,
    paddingHorizontal: 32,
    paddingVertical: 20,
    justifyContent: 'center'
  },
  title: {
    fontFamily: 'circularstd-medium',
    marginVertical: 4,
    fontSize: fontSizes['x-lg'],
    color: colors.dark.text
  },
  upload: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: colors.separator
  },
  upload_icon: {
    alignSelf: 'center'
  },
  upload_title: {
    color: colors.dark.text,
    flex: 4,
    alignSelf: 'center',
    marginLeft: 10
  },
  back: {
    flexDirection: 'row',
    marginTop: 105  
  }
})
