import React, {Component} from 'react';
import {
	View, 
	Text,
	ScrollView,
	StyleSheet
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { LinearGradient } from 'expo'

import Button from '../components/Button';
import { colors, fontSizes, gradients } from '../constants/styles'

export default class TermsAndConditions extends React.Component {
	constructor(props){
	  super(props);
	}

	onSubmit(){
		Actions.phoneverification();
	}

	render(){
		return(
			<View style={styles.container}>
				<Text style={styles.title}> Terms of use </Text>

				<ScrollView>
					<Text style={styles.content}>A Terms and Conditions agreement, also known as a T & C or Terms of Use or Terms of Service, is the legal backbone of the relationship between your mobile app and yours users.</Text>
					<Text style={styles.content}> It's an incredibly important legal agreement for you to have. It doesn't matter if your apps are for iOS, Android, Windows Phone or BlackBerry.    </Text>
					<Text style={styles.content}> It sets forth caluses that embody the rules, requirements, restrictions and limitations that a user must agree to in order to use your mobile app.</Text>
					<Text style={styles.content}> Your mobile app needs a Terms and Conditions if you want to stoop abusive users, terminate access or accounts at your sole discretion, and anforce rules and guidelines that you set. It's the set of rules that users must follow to access, use and continue to use your app.</Text>
					<Text style={styles.content}>Your mobile app needs a Terms and Conditions if you want to stop abusive users, terminate access or accounts at your sole discretion, and enforce rules and guidelines that you set.</Text>
				</ScrollView>
				<LinearGradient
		         style={styles['bottomCard']}
		         colors={gradients.whiteGray}>
		          <Button size='lg' onPress={this.onSubmit} >Agree and Continue</Button>
		        </LinearGradient>
				
			</View>
		)
	}
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 32,
    marginTop: 16,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  title: {   
  	height: 24,
  	fontFamily: 'circularstd-medium',
  	fontSize: fontSizes['lg'],
  	fontWeight: "500",  	
  	letterSpacing: 0,
  	textAlign: "left",
  	color: "#3b4859",
  	marginHorizontal: 8
  },
  content: {
  	  color: colors.text,
	  marginHorizontal: 8,
	  marginVertical: 16	  
  },
   bottomCard: {
    paddingBottom: 16,
    justifyContent: 'center'
  }

})