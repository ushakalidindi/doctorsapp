import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList
} from 'react-native';
import moment from 'moment';
import { Actions } from 'react-native-router-flux';

import { colors, fontSizes } from '../constants/styles'

import Button from '../components/Button'
import NotificationRow from '../components/NotificationRow'
import FeedbackCard from '../components/FeedbackCard'

import BottomNavigation from '../components/BottomNavigation'

export default class NotificationsList extends React.Component {
  constructor(props) {
    super(props);
    let now = moment(new Date());

    this.state = {
      chats : [
        {
          name: 'Raymond Romero',
          lastMessage: {message: "Dr. Stephen Colins has accepted your appointent request", createdAt: moment(now).subtract(30, 'minutes')},
          unread: [],
        },
        {
          name: 'Russell Clarke',
          lastMessage: {message: "Dr. Adam Romero has changed the call timings to 3PM 2nd Jan, Saturday", createdAt: moment(now).subtract(1, 'day')},
          unread: [],
        },
        {
          name: 'Lola Fox',
          lastMessage: {message: "Video Call with Dr. Stephen Collinds will start in another 20 mins", createdAt: moment(now).subtract(2, 'months')},
          unread: [],
        }
      ]
    }
  }

 

  _renderItem = ({item, index}) => {
    return (
      <NotificationRow data={item}/>
    )
  }

  render() {
    return (
      <View style={styles['container']}>
        <FlatList
          keyExtractor={(item, index) => 'key'+index}
          style={styles.list}
          data={this.state.chats}
          renderItem={this._renderItem}/>
        <BottomNavigation type="patient" active={0} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1
  },

  list: {
    flex: 1
  }
})