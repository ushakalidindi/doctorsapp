import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Image
} from 'react-native';

import { colors, fontSizes, gradients } from '../../constants/styles'
import CustomButton from '../../components/CustomButton'

import DoctorCard from '../../components/DoctorCard'
import BottomNavigation from '../../components/BottomNavigation'

import SortMenu from '../../components/SortMenu'

import Filter from '../../assets/images/filter.png'
import SortingArrow from '../../assets/images/sorting-arrows.png'
import { Actions } from 'react-native-router-flux';
import * as actions from '../../actions';
import { connect } from 'react-redux';

class DoctorsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [
        {
          name: "Dr. Jeff Mason",
          avatar: "https://randomuser.me/api/portraits/women/68.jpg",
          rating: 4.2,
          language: ['German'],
          location: "Munich, Germany",
          repliesIn: 2,
          
          experience: "20+",
          isOnline: true
        },
        {
          name: "Dr. Mark Woods",
          avatar: "https://randomuser.me/api/portraits/men/48.jpg",
          rating: 4,
          language: ['Ukrainian '],
          location: "Kiev, Ukraine",
          repliesIn: 4,
          experience: "2+",
          isOnline: true
        },
        {
          name: "Dr. Mark Twain",
          avatar: "https://randomuser.me/api/portraits/men/18.jpg",
          rating: 4.2,
          language: ['Spanish'],
          location: "Madrid, Spain",
          repliesIn: 2,
          
          experience: "20+",
          isOnline: true
        },
        {
          name: "Dr. Jane Dos",
          avatar: "https://randomuser.me/api/portraits/women/29.jpg",
          rating: 4,
          language: ['Maori'],
          location: "Kingston, New Zealand",
          repliesIn: 4,
          experience: "2+",
          isOnline: true
        },
                {
          name: "Dr. Xang Yeng",
          avatar: "https://randomuser.me/api/portraits/men/90.jpg",
          rating: 4,
          language: ['Japaneese'],
          location: "Tokyo, Japan",
          repliesIn: 4,
          experience: "2+",
          isOnline: true
        },
        {
          name: "Dr. Jeffery Mark",
          avatar: "https://randomuser.me/api/portraits/women/53.jpg",
          rating: 4.2,
          language: ['English'],
          location: "Maide, Singapore",
          repliesIn: 2,
          
          experience: "20+",
          isOnline: true
        },
        {
          name: "Dr. Yen Huai",
          avatar: "https://randomuser.me/api/portraits/men/20.jpg",
          rating: 4,
          language: ['Chineese'],
          location: "Bejing, China",
          repliesIn: 4,
          experience: "2+",
          isOnline: true
        },
        {
          name: "Dr. Jeena Den",
          avatar: "https://randomuser.me/api/portraits/men/18.jpg",
          rating: 4.2,
          language: ['Spanish'],
          location: "Taico , Mexico",
          repliesIn: 2,
          
          experience: "20+",
          isOnline: true
        },
        {
          name: "Dr. Dennis Michail",
          avatar: "https://randomuser.me/api/portraits/men/5.jpg",
          rating: 4,
          language: ['Portuguese '],
          location: "Lewis, Brazil",
          repliesIn: 4,
          experience: "2+",
          isOnline: true
        },
                {
          name: "Dr. Jenifer Braid",
          avatar: "https://randomuser.me/api/portraits/men/78.jpg",
          rating: 4,
          language: ['Spanish'],
          location: "Cary, Argentina",
          repliesIn: 4,
          experience: "2+",
          isOnline: true
        }
      ],
    
      Doctors_List: [
        {
          name: props.Doctors_List && props.Doctors_List.first_name,
          avatar: props.Doctors_List && props.Doctors_List.avatar,
          rating: props.Doctors_List && props.Doctors_List.overAllRatings,
          language: ["English"],
          location: props.Doctors_List && props.Doctors_List.res_address,
          repliesIn: props.Doctors_List && props.Doctors_List.responseTime,
          
          experience: props.Doctors_List && props.Doctors_List.experience + " +",
          isOnline: true
        }],

      isSortOpen : false,
      isFilterOpen: false,
      sortKey: 'rating'//TODO
    },

    this._onSortSubmit = this._onSortSubmit.bind(this);
  }

  _onRemoveFromFavorites = (item) => {
    // console.log(item);
    // Actions.push("patient_confirm_pay");
  }

  
  _onSortSubmit(sortKey) {
    this.setState({sortKey, isSortOpen: false});
  }

  _onViewDoctor = (item) => {
    console.log("item.....", item);
    Actions.doctor_details({doctorDetails: item});
  }

  filterOpen(){
    Actions.filter_doctors();
  }

  _renderDoctors = ({item, index}) => {
    return (
      <View style={styles.card}>
      <TouchableOpacity onPress={this._onViewDoctor.bind(this, item)}>  
        <DoctorCard data={item} />
       </TouchableOpacity> 
      </View>
    )
  }

  render() {
     let sortMenu = this.state.isSortOpen ? <SortMenu defaultKey={this.state.sortKey} onSubmit={this._onSortSubmit} />  : null
     let doctorlist = [{
          name: this.props.Doctors_List && this.props.Doctors_List.first_name,
          avatar: this.props.Doctors_List && this.props.Doctors_List.avatar,
          rating: this.props.Doctors_List && this.props.Doctors_List.overAllRatings,
          language: ["English"],
          location: this.props.Doctors_List && this.props.Doctors_List.res_address,
          repliesIn: this.props.Doctors_List && this.props.Doctors_List.responseTime,
          
          experience: this.props.Doctors_List && this.props.Doctors_List.experience + " +",
          isOnline: true
     }];
    return (
      <View style={styles['container']}>
        <ScrollView>
          <View style={styles.buttons}>
           
            <View style={{justifyContent: 'center', flex: 1, flexDirection: 'row'}}>
              <CustomButton title="SORT" onPress={() => {this.setState({isSortOpen : true})}}></CustomButton>              
            </View>
            <View style={{justifyContent: 'center', flex: 1, flexDirection: 'row'}}>
              <CustomButton title="FILTER" onPress={this.filterOpen}></CustomButton>             
            </View>
          </View>

          <FlatList
            keyExtractor={(item, index) => 'key'+index}
            style={styles.list}
            data={this.props.Doctors_List}
            renderItem={this._renderDoctors}>
          </FlatList>
 
        </ScrollView>
        <BottomNavigation type="patient" active={0} />
        {sortMenu}
      </View>
    )
  }
}

function mapStateToProps({fetchInput}){
  if(fetchInput != null){
    console.log("fetchInput.....", fetchInput.doctors_list);
    let list = fetchInput.doctors_list;
     return {Doctors_List: list}
  }
  return {test: "test"}
}

export default connect(mapStateToProps, actions)(DoctorsList);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    flex: 1,
    paddingBottom: 70
  },
  card: {
    marginHorizontal: 32,
    marginVertical: 8
  },
  list: {
    marginTop: 16
  },
  button: {
    padding: 16,
    backgroundColor: colors.white
  },
  buttonText: {
    fontSize: fontSizes['sm'],
    color: colors.dark.text
  },
  buttons: {
    flexDirection: "row",
    alignItems: "flex-start"
  },
  feedbackForm: {
    padding: 16
  }
})
