import { combineReducers } from 'redux';
import userToken from './userToken_reducer';
import member_logged from './member_logged_in';
import fetchInput from './fetch_profile_data';
import favouriteDoctor from './favourite_doctors';
import patient_appointments from './fetch_patient_appointments';
import past_appointments from './fetch_past_patient_appointments';
import upcomming_appointments from './fetch_upcomming_patient_appointments';

export default combineReducers({
  userToken,
  member_logged,
  fetchInput,
  favouriteDoctor,
  patient_appointments,
  past_appointments,
  upcomming_appointments
});
