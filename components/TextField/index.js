import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text
} from 'react-native';

import { colors, fontSizes } from '../../constants/styles'

class TextField extends Component {
  constructor(props) {
     super(props);
     this.state={
      field_name: props.prop_name,
      input_value: '',
      label: props.label,
      style: props.style,
      inputStyle: props.inputStyle,
      placeholder: props.placeholder,
      placeholderTextColor: props.placeholderTextColor
    }
    this._onChangeText = this._onChangeText.bind(this);
  }

  _onChangeText(textInput){
      let textValue = textInput;
      this.setState({input_value: textValue});
      this.props.onChange(this.state.field_name, textValue);
  
  }

  render(){
    let label = null;
    if(this.state.label) label = (<Text style={styles.label}>{this.state.label.toUpperCase()}</Text>);
    return (
      <View style={[styles['container'], this.state.style]}>
        {label}
        <TextInput
          onChangeText={this._onChangeText}
          value={this.state.input_value}
          underlineColorAndroid={'transparent'}
          style={[styles['input'], this.state.inputStyle]}
          placeholder={this.state.placeholder || ''}
          placeholderTextColor={this.state.placeholderTextColor || colors.lightText}
          
        ></TextInput>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {

  },
  input: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.dark.background,
    padding: 8,
    height: 50
  },
  label: {
    fontSize: 13,
    color: colors.text,
    marginVertical: 4
  }
})

export default TextField