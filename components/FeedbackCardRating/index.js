import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import { LinearGradient } from 'expo';
import Rating from 'react-native-rating'
import { Actions } from 'react-native-router-flux';

import { colors, fontSizes, gradients } from '../../constants/styles'

import TextField from '../TextField'
import Button from '../Button'
import Favorite from '../SvgIcons/Favorite'

const images = {
  starFilled: require('../../assets/images/rating_full.png'),
  starUnfilled: require('../../assets/images/rating_empty.png')
}
export default class FeedbackCardRating extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      feedback: ''
    }
  }

  _onClose = () => {
    Actions.patient_profile();
  }

  _onSubmit = () => {
    Actions.patient_profile();
  }

  render() {


    return (
      <LinearGradient colors={gradients.greenBlue} style={styles.container}>
        <View style={[styles.wrapper]}>
          <Text style={{color: colors.white}}>Can you rate us on the app store?</Text>
        </View>
        <View style={{alignSelf: 'center', marginVertical: 16, flexDirection: 'row'}}>
            <Favorite fill={colors.white} style={[styles.gap]} />
            <Favorite fill={colors.white} style={[styles.gap]} />
            <Favorite fill={colors.white} style={[styles.gap]} />
            <Favorite fill={colors.white} style={[styles.gap]} />
            <Favorite fill={colors.white} style={[styles.gap]} />
        </View>
        <View style={[styles.wrapper, styles.buttonsContainer]}>
          <Button style={{borderWidth: 1, width: 120}} onPress={this._onClose} background="transparent" border={colors.borderWhite} size="md">
            Not Now
          </Button>
          <TouchableOpacity onPress={this._onSubmit} style={styles.submit}>
            <Text style={{color: colors.white}}>Yes</Text>
          </TouchableOpacity>
        </View>
      </LinearGradient>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 30,
    padding: 16,
    marginHorizontal: 32
  },
  wrapper: {
    marginVertical: 8
  },
  textInput: {
    borderColor: colors.borderWhite,
    backgroundColor: 'transparent'
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  submit: {
    backgroundColor: colors.dark.text,
    borderWidth: 1,
    borderColor: colors.dark.text,
    paddingVertical: 16,
    paddingHorizontal: 28,
    borderRadius: 4
  },
  gap: {
    paddingLeft: 5,
    paddingRight: 5
  }
})
