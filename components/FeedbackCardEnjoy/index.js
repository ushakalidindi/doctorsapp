import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import { LinearGradient } from 'expo';
import { Actions } from 'react-native-router-flux';
import { colors, fontSizes, gradients } from '../../constants/styles'

import TextField from '../TextField'
import Button from '../Button'

export default class FeedbackCardEnjoy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      feedback: ''
    }
  }

  _onClose = () => {
    Actions.patient_profile();
  }

  _onSubmit = () => {

  }

  render() {
    return (
      <LinearGradient colors={gradients.greenBlue} style={styles.container}>
        <View style={[styles.wrapper]}>
          <Text style={{color: colors.white}}>Hey Raj are you enjoying the app?</Text>
        </View>
        <View style={[styles.wrapper, styles.buttonsContainer]}>
          <Button style={{borderWidth: 1, width: 120}} onPress={this._onClose} background="transparent" border={colors.borderWhite} size="md">
            No
          </Button>
          <Button style={{borderWidth: 1, width: 120}} onPress={this._onClose} background="transparent" border={colors.borderWhite} size="md">
            Yes
          </Button>
          
        </View>
      </LinearGradient>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 30,
    padding: 16,
    marginHorizontal: 32
  },
  wrapper: {
    marginVertical: 8
  },
  textInput: {
    borderColor: colors.borderWhite,
    backgroundColor: 'transparent'
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  submit: {
    backgroundColor: colors.dark.text,
    borderWidth: 1,
    borderColor: colors.dark.text,
    paddingVertical: 16,
    paddingHorizontal: 28,
    borderRadius: 4
  }
})
