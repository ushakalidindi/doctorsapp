import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  PixelRatio
} from 'react-native';

import { colors, sizes, fontSizes } from '../../constants/styles'
import { CustomText } from '../CustomText'
import Icon from 'react-native-vector-icons/FontAwesome';

export const DoctorCardList = (props) => (
  <View style={styles.container}>
      <View style={styles.top}>
        <Image style={styles.image} source={{uri: props.avatar}} />
        <View style={styles.nameContainer}>
          <CustomText style={styles.name}>{props.name}</CustomText>
          <CustomText>{props.language}</CustomText>
        </View>
        <View style={styles.rating}>
          <CustomText><Icon name={"star"} size={20} color={colors.text} /> {props.rating}</CustomText>
        </View>
      </View>
      <View style={styles.mid}>
        <CustomText>{props.location}</CustomText>
      </View>
      <View style={styles.bottom}>
        <CustomText>replies in {props.responseTime} mins</CustomText>
        <CustomText>{props.experience}+ years of exp</CustomText>
      </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    padding: 20,
    shadowColor: colors.shadow,
    shadowOpacity: 0.06,
    shadowRadius: 10,
    shadowOffset: {x: 0, y: 1},
    marginBottom: 20
  },

  top: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  image: {
    width: 60,
    height: 60,
    borderRadius: 60/2
  },

  nameContainer: {
    flex: 4,
    marginLeft: 10
  },

  name: {
    color: colors.dark.text,
    fontSize: fontSizes.subTitle,
    marginBottom: 5
  },

  rating: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },

  mid: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },

  bottom: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }

})

export default DoctorCardList
