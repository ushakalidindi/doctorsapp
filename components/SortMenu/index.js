import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Modal,
  TouchableOpacity
} from 'react-native';

import { colors, sizes, fontSizes } from '../../constants/styles'
import { CustomText } from '../CustomText'

class SortMenu extends React.PureComponent {
  constructor(props) {
      super(props);
      this.state = {
        activeKey : props.defaultKey || 'rating',
      }

      this.keys = {
        'rating': 'Popularity',
        'experience': 'Years of experience',
        'responseTime': 'Response time'
      }
  }

  _onSortPicked(activeKey) {
    this.setState({activeKey}, () => {
      this.props.onSubmit(activeKey);
    })
  }

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        onRequestClose={() => this._onSortPicked(this.state.activeKey)}
        visible={true}>
        <View style={styles.overlay}>
          <View style={styles.container}>
            <CustomText style={{padding: 20}}>SORT BY</CustomText>
            { Object.keys(this.keys).map(
              (key, i) => <TouchableOpacity key={i} onPress={() => this._onSortPicked(key)}>
                  <CustomText bold={true} style={[styles.text, this.state.activeKey === key && styles.active]}>
                  {this.keys[key]}
                </CustomText>
              </TouchableOpacity>
              )
            }
          </View>
        </View>
      </Modal>
    )
  }

}

const styles = StyleSheet.create({
    overlay: {
      ...StyleSheet.absoluteFillObject,
      backgroundColor: colors.overlay,
      zIndex: 1000
    },

    container: {
      backgroundColor: colors.white
    },

    text : {
      padding: 20,
      borderBottomWidth: 1,
      borderBottomColor: colors.background
    },

    active: {
      color: colors.green,
      borderLeftWidth: 5,
      borderLeftColor: colors.green
    }
})

export default SortMenu
