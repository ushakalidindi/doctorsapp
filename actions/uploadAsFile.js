import firebase from 'firebase';

import {
  USER_TOKEN,
  CLEAR_USER_ID
} from './types';


export const uploadAsFile = (uri) => async dispatch => {

  console.log("data", uri);
  const response = await fetch(uri);
  const blob = await response.blob();
  var metadata = {
    contentType: 'image/jpeg',
  };

  let name = new Date().getTime() + "-media.jpg"
  const ref = firebase
    .storage()
    .ref()
    .child('assets/' + name)

    let nameurl = "IMG-20180727-WA0000.jpg";
    const refurl = firebase.storage().ref(`assets/${nameurl}`)
    
    const task = ref.put(blob, metadata);

   await task.on('state_changed', function(snapshot) {
      //  progressCallback && progressCallback(snapshot.bytesTransferred / snapshot.totalBytes)

        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');
    })  
    
    await refurl.on('value', function(snapshot){
      console.log('snap...', snapshot);
        const downloadURL = refurl.getDownloadURL();
        console.log("_uploadAsByteArray ", downloadURL);
        var ref1 = firebase.database().ref('assets');
        ref1.push({
          'URL': downloadURL,
          'name': name,
          'when': new Date().getTime()
        })
    })
        
  dispatch({ type: CLEAR_USER_ID });
  
}
