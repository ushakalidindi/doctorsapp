import {
  FETCH_FAVOURITE_DOCTORS
} from './types';

import { AsyncStorage } from 'react-native';
import firebase from 'firebase';

export const updateFavouriteDoctor = (data) => async dispatch => {
		
	  const Data = {
	    patient_id: data.patient_id,
	    doctor_id: data.doctor_id,
	    favourite: data.favourite	    
	  }

      console.log('patientAppointment....', Data);

      let ref = firebase.database().ref(`patients/favourite`);
	
	
	ref.orderByChild("patient_id").equalTo(data.patient_id).on("value", function(snapshot) {
       console.log('MEMBER_LOGGED=--------',snapshot.key);
       
       if(snapshot.val() != null || snapshot.val() != undefined){
	       let arrycurrentPatients = Object.values(snapshot.val());   
	       arrycurrentPatients.map((list,i) => {
	       		if(data.doctor_id == list.doctor_id){
	       			ref.orderByChild("doctor_id").equalTo(data.doctor_id).on("child_added", function(snapshotvalue) {
				       console.log('key=--------',snapshotvalue.key);
				       let currentKey = snapshotvalue.key;
				       firebase.database().ref(`patients/favourite/${currentKey}`).update({
				        "favourite": data.favourite
				      });
				   });
	       		}
	       		else{

	       			firebase.database().ref(`patients/favourite`)
	          			.push(Data);
	       		}
	       })
	    }
	    else{
	    	firebase.database().ref(`patients/favourite`)
	          			.push(Data);
	    }
       
   });
}
/*
export const FetchFavouriteDoctors = () => async dispatch => {
	//let user_id =  await AsyncStorage.getItem('user_id');
	let ref = firebase.database().ref(`patients/favourite`);
	let ref1 = firebase.database().ref(`doctors/Signup`);
	let ref2 = firebase.database().ref(`patients/Signup`);

	let phone_number = await AsyncStorage.getItem('phone_number');
	console.log("phone.....", phone_number);
	await ref2.orderByChild("phone_number").equalTo(phone_number).on("value", function(snapshot){
		console.log("snapshot...", snapshot.val());
		if(snapshot.val() != null || snapshot.val() != undefined){
			let user_id = Object.values(snapshot.val()[0].user_id);
				console.log("user_id...", user_id);

			ref.orderByChild("patient_id").equalTo(user_id).on("value", function(snapshot) {
			
			 if(snapshot.val() != null || snapshot.val() != undefined){
				let arrycurrentPatients = Object.values(snapshot.val());  
				let newResultArray = [];
				let mergedObj = {};	
				console.log("arrycurrentPatients....", arrycurrentPatients);
				arrycurrentPatients.map((list,i) => {
					if(list.favourite == true){
						let obj = {};        
				        ref1.orderByChild("doctor_id").equalTo(list.doctor_id).on("value", function(snap) {
				          if(snap.val() != null || snap.val() != undefined){  
				            let presentfavourite = Object.values(snap.val());    
				            mergedObj = Object.assign(data, presentfavourite[0]); 
				            obj.user_id = mergedObj.user_id,
				            obj.name = mergedObj.first_name,
				          	obj.avatar = mergedObj.avatar,
				          	obj.rating = mergedObj.overAllRatings,
				          	obj.language = ["English"],
				          	obj.location = mergedObj.res_address,
				          	obj.repliesIn = mergedObj.responseTime,
				          	obj.experience = mergedObj.experience + "+",
				          	obj.specialization = mergedObj.specialization,
				          	obj.isOnline = true		//need to be fetched
				            newResultArray.push(obj);
				            console.log("newResultArray....", newResultArray);
				            dispatch({ type: FETCH_FAVOURITE_DOCTORS, payload: newResultArray });
				          }
				        });
					}
				}) 
			  }
			})
		}
	})	
}

*/