import {
  FETCH_PATIENT_APPOINTMENT
} from './types';

import { AsyncStorage } from 'react-native';
import firebase from 'firebase';

export const fetchPatientAppointment = (patient_id) => async dispatch => {
	var ref = firebase.database().ref(`users/PatientAppointments`);
	let ref2 = firebase.database().ref(`doctors/Signup`);
	let patientId = patient_id;

	await ref.orderByChild("patient_id").equalTo(patientId).on("value", function(snapshot) {
        console.log("snapshot...", snapshot.val());
		if(snapshot.val() != null || snapshot.val() != undefined){
			let arrPatientAppointments = Object.values(snapshot.val());
			let arrayRequestedAppointments = [];
			arrPatientAppointments.map((data,i) => {
				obj = {};
				if(data.status == "requested"){
					ref2.orderByChild("doctor_id").equalTo(data.doctor_id).on("value", function(doctorval){
						if(doctorval.val() != null || doctorval.val() != undefined){
							let arrPatientAppointments = Object.values(doctorval.val())[0];
							obj.first_name = arrPatientAppointments.first_name;
							obj.last_name = arrPatientAppointments.last_name;
							obj.avatar =arrPatientAppointments.avatar;
							obj.specialization = arrPatientAppointments.specialization;
							
							obj.type = data.type;
							obj.schedule_date = data.schedule_date;
							obj.schedule_time = data.schedule_time;
							arrayRequestedAppointments.push(obj);
							dispatch({ type: FETCH_PATIENT_APPOINTMENT, payload: arrayRequestedAppointments });
						}
					});  
				}
			});
		}    	       
    });  
}



export const cancelPatientAppoitment = (data) => async dispatch => {
	var ref = firebase.database().ref(`users/PatientAppointments`);
	let patientId = data.patient_id;
	let doctorId = data.doctor_id;

	await ref.orderByChild("patient_id").equalTo(patientId).on("value", function(snapshot) {
        console.log("snapshot...", snapshot.val());
		if(snapshot.val() != null || snapshot.val() != undefined){
			let arrPatientAppointments = Object.values(snapshot.val());
			let arrayRequestedAppointments = [];
			arrPatientAppointments.map((data,i) => {
				ref.orderByChild("doctor_id").equalTo(doctorId).on("child_added", function(doctorval){
					if(doctorval.val() != null || doctorval.val() != undefined){
						let currentKey = doctorval.key;
						console.log("currentKey...", currentKey);
				        firebase.database().ref(`users/PatientAppointments/${currentKey}`).update({
					        status: "Cancelled"
				        });
					}
				});				
			});
		}    	       
    });  
}